<?php

namespace App\Controllers;

use GuzzleHttp\Client;
use App\Controllers\DbController;
use \DB;


class CoinController 
{
    const COIN_API_URL = 'https://min-api.cryptocompare.com/data/price';
    const DB_NAME = 'laba4';
    const DB_USER_TABLE = 'coins';

    public function getCoinPrice(array $params = []): ?float
    {
        $client = $this->getList();

        $responseContent = $client->request('GET', '', [
            'query'=>$params])->getBody()->getContents();

        $data = json_decode($responseContent);
        if (isset($data->Response)) {
            return null;
        }
        else {
            return $data->USD;
        }
    }

    private function getList()
    {
        return new Client([
            'base_uri' => self::COIN_API_URL,
        ]);
    }

    

    public function addCoin(array $params = [])
    {
        $dbController = new DbController();
        $dbController->createDbInstance(self::DB_NAME);

        DB::insert(self::DB_USER_TABLE, $params);
    } 

    
    

    public function getCoins()
    {
        $dbController = new DbController();
        $dbController->createDbInstance(self::DB_NAME);

        return DB::query("SELECT * FROM " . self::DB_USER_TABLE );
    }

    public function updateCoin($params)
    {
        $dbController = new DbController();
        $dbController->createDbInstance(self::DB_NAME);

        return DB::query("UPDATE " . self::DB_USER_TABLE . " SET price=".$params["price"]." WHERE idcoins=".$params["idcoins"]);
    }

    public function getCoinName($id) {
        $dbController = new DbController();
        $dbController->createDbInstance(self::DB_NAME);

        return DB::query("SELECT coinName FROM " . self::DB_USER_TABLE ." WHERE idcoins=".$id)[0]["coinName"];
    }

    public function deleteAllCoins() {
        $dbController = new DbController();
        $dbController->createDbInstance(self::DB_NAME);

        return DB::query("DELETE FROM " . self::DB_USER_TABLE);
    }
}

