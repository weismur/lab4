<?php
namespace App;

require __DIR__ . '/vendor/autoload.php';

use App\Controllers\CoinController;


$coin = new coinController();

$method = $_REQUEST['method'];

$json = file_get_contents("php://input");
$data = json_decode($json);

$methods = [
    'addcoin' => 'App\addCoinHandle',
    'getcoins' => 'App\getCoinsHandle',
    'updatecoin' => 'App\UpdateCoinHandle',
    'deletecoin' => 'App\deleteAllhandle',
];

if (empty($methods[$method]) || !function_exists($methods[$method])) {
    http_response_code(405);
    var_dump(function_exists('App\\'. $methods[$method]));
    echo 'Method ' . $method . ' doesn\'t exitst';
    return;
}

echo $methods[$method]($data);


function addCoinHandle($data)
{
    print_r($data);
    $params = [
        'fsym' => $data->coin,
        'tsyms' => "USD"
    ];

    $coinController = new CoinController();
    $coinPrice = $coinController->getCoinPrice($params);

    if ($coinPrice == null) {
        http_response_code(412);
        echo 'Сервер не может обработать условие';
        return;
        
    }
    else {
        $bdparams = [
            'coinName' => $data->coin,
            'price' => $coinPrice,
        ];

        $coinController->addCoin($bdparams);
    }
}

function getCoinsHandle() {
    $coinController = new CoinController();
    print_r($coinController->getCoins());
}

function UpdateCoinHandle($data) {
    if (!is_int($data->id)) 
    { 
        http_response_code(415);
        echo 'Формат запрашиваемых данных не поддерживается сервером, поэтому запрос отклонён';
        return;
    }

    $coinController = new CoinController();
    $coinName = $coinController->getCoinName($data->id);
    $params = [
        'fsym' => $coinName,
        'tsyms' => "USD"
    ];

    $coinController = new CoinController();
    $coinPrice = $coinController->getCoinPrice($params);

    if ($coinPrice == null) {
        http_response_code(412);
        echo 'Сервер не может обработать условие';
        return;
    }
    else {
        $bdparams = [
            'price' => $coinPrice,
            'idcoins' => $data->id,
        ];
        $coinController->updateCoin($bdparams);
    }
}

function deleteAllhandle() {
    $coinController = new CoinController();
    $coinController->DeleteAllCoins();
}

